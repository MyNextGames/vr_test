﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy: PoolableMonoBehaviour
{
    private const float      DESTORY_DELEY   = 3.0f;

    [SerializeField]
    private float            APPROACH_SPEED  = 2.0f;

    [Space]

    [SerializeField]
    private EnemyCore        CORE        = null;

    private float            mTimer      = -1.0f;

    protected override void reset()
    {
        mTimer = -1.0f;
        CORE.reset( this );   
    }

    private void Update()
    {
        if ( !Hud.Instance.isGameOver() ) {
            if ( mTimer >= 0.0f ) {
                mTimer -= Time.deltaTime;
                if ( mTimer < 0.0f ) {
                    mTimer = -1.0f;
                    retunToPool( );
                }
            } else {
                Vector3 d = Gun.Instance.transform.position - transform.position;
                if ( d.sqrMagnitude > 0.3f * 0.3f ) {
                    d.Normalize( );
                    transform.position += d * APPROACH_SPEED * Time.deltaTime;
                } else {
                    Debug.Log("DAMAGE!!!!!");
                    Hud.Instance.damage( );
                    retunToPool( );
                }
            }
        }
    }

    public bool isDamaged()
    {
        return ( mTimer > 0.0f );
    }

    public void damage()
    {
        Hud.Instance.addScore( 10 );
        mTimer = DESTORY_DELEY;
        Debug.Log("CCCCCCCCCCC");
    }
}
