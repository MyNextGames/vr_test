﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Room : MonoBehaviour
{
    public void hit( BaseEventData eventData )
    {
        PointerEventData ped = eventData as PointerEventData;
        if ( ped != null ) {
            if ( ped.button != PointerEventData.InputButton.Left ) {
                return;
            }
        }

        Gun.Instance.Shake( );
    }
}
