﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
public class Hud : MonoBehaviour
{
    public static Hud Instance { get; private set; }

    [SerializeField]
    private Animation       DAMAGE_ANIM     = null;
    [SerializeField]
    private GameObject      GAME_OVER_INFO  = null;
    [SerializeField]
    private Text            SCORE_TEXT      = null;
    [SerializeField]
    private Transform       BAR_SCALE_OBJ   = null;

    private float       mHp             = 1.0f;
    private int         mPoints         = 0;

    private void Awake()
    {
        if ( Instance != null ) Debug.LogError("Hud instance already exists in the scene! [Obj: " + Instance.gameObject.name + "]");
        else Instance = this;

        UpdateUI( );
    }

    public void damage()
    {
        if ( !isGameOver( ) ) {
            DAMAGE_ANIM.Play( );
            mHp -= 0.1f;
            if ( mHp <= 0.0f ) {
                GAME_OVER_INFO.SetActive(true);
            }

            UpdateUI( );
        }
    }

    public void addScore( int points )
    {
        mPoints += points;
        UpdateUI( );
    }

    private void UpdateUI()
    {
        SCORE_TEXT.text = "Score: " + mPoints;
        Vector3 s = new Vector3( mHp, 1.0f, 1.0f );
        BAR_SCALE_OBJ.localScale = s;
    }

    public bool isGameOver()
    {
        return ( mHp <= 0.0f );
    }
}
