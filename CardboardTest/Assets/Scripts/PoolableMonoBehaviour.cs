﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PoolableMonoBehaviour: MonoBehaviour
{
    private ObjectPool mReturnPool      = null;

    private void _reset( ObjectPool returnPool )
    {
        mReturnPool = returnPool;
        reset( );
        gameObject.SetActive(true);
    }

    protected void retunToPool()
    {
        gameObject.SetActive(false);
        mReturnPool.retunToPool(this);
    }

    protected abstract void reset();

    public class ObjectPool
    {
        public delegate PoolableMonoBehaviour SpawnObjCall();
        public delegate void OnObjReturnd( PoolableMonoBehaviour obj );

        private Queue< PoolableMonoBehaviour >     mPooledObjs          = new Queue< PoolableMonoBehaviour >();

        private SpawnObjCall                    mSpawnObjCall   = null;
        private OnObjReturnd                    mOnObjReturnd   = null;
        public ObjectPool( int initPoolCount, SpawnObjCall spawnMethod, OnObjReturnd retunCallback = null )
        {
            mSpawnObjCall = spawnMethod;
            mOnObjReturnd = retunCallback;

            for ( int i = 0; i != initPoolCount; i++ ) {
                mPooledObjs.Enqueue(mSpawnObjCall( ));
            }
        }

        public PoolableMonoBehaviour getObj()
        {
            if ( mPooledObjs.Count <= 0 ) mPooledObjs.Enqueue(mSpawnObjCall( ));

            PoolableMonoBehaviour obj = mPooledObjs.Dequeue( );
            obj._reset(this);
            return obj;
        }

        public void retunToPool( PoolableMonoBehaviour obj )
        {
            obj.gameObject.SetActive(false);
            mPooledObjs.Enqueue(obj);
            if ( mOnObjReturnd != null ) mOnObjReturnd(obj);
        }
    }
}

