﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class EnemyCore: MonoBehaviour
{
    [SerializeField]
    private Material        BASE_MAT    = null;
    [SerializeField]
    private Material        GAZE_MAT    = null;
    [SerializeField]
    private ParticleSystem  EXPLOSION   = null;

    [Space]

    [SerializeField]
    private AnimationClip   SHOW_ANIM   = null;
    [SerializeField]
    private AnimationClip   HIDE_ANIM   = null;

    private Animation       mAnim       = null;

    private Enemy           mPooledObj  = null;

    private Renderer myRenderer;

    public void setGazedAt( bool gazedAt )
    {
        if ( BASE_MAT != null && GAZE_MAT != null ) {
            if ( gazedAt ) myRenderer.material = GAZE_MAT;
            else myRenderer.material = BASE_MAT;
            return;
        }
    }

    public void hit( BaseEventData eventData )
    {
        if ( mPooledObj.isDamaged() ) return;

        PointerEventData ped = eventData as PointerEventData;
        if ( ped != null ) {
            if ( ped.button != PointerEventData.InputButton.Left ) {
                return;
            }
        }

        Gun.Instance.Shake( );
        EXPLOSION.gameObject.SetActive( true );
        EXPLOSION.Play( );
        mPooledObj.damage( );
        mAnim.Play( "Hide" );
    }

    private void init()
    {
        if ( mAnim == null ) {
            mAnim = GetComponent<Animation>( );
            myRenderer = GetComponent<Renderer>( );
            mAnim.AddClip(SHOW_ANIM, "Show");
            mAnim.AddClip(HIDE_ANIM, "Hide");
        }
    }

    private void Start()
    {
        init( );
    }

    private void OnEnable()
    {
        init( );
        mAnim.Play("Show");
    }

    public void reset( Enemy owner )
    {
        init( );
        setGazedAt(false);
        EXPLOSION.gameObject.SetActive(false);
        mPooledObj = owner;
    }
}
