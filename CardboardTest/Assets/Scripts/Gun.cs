﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleVR.Demos;

public class Gun : MonoBehaviour
{
    public static Gun Instance { get; private set; }
        
    [SerializeField]
    private Animation   SHAKE_ANIM      = null;

    public void Shake()
    {
        if ( !Hud.Instance.isGameOver( ) ) {
            SHAKE_ANIM.Play( );
        }
    }

    private void Awake()
    {
        if ( Instance != null ) Debug.LogError("Gun instance already exists in the scene! [Obj: " + Instance.gameObject.name + "]");
        else Instance = this;
    }

}
